package com.spotmy.spotmy.dtos.music;


import java.util.UUID;

public record ResponseMusicDTO(
        UUID id,
        String title,
        String artist,
        String genre,
        int duration
){}