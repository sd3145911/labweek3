package com.spotmy.spotmy.dtos.music;


public record RequestMusicDTO(
        String title,
        String artist,
        String genre,
        int duration
) {}
