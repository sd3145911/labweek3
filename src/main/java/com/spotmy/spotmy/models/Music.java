package com.spotmy.spotmy.models;

import com.spotmy.spotmy.dtos.music.RequestMusicDTO;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@Entity(name = "music")
public class Music {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    private String title;
    private String artist;
    private String genre;
    private int duration;

    public Music(RequestMusicDTO musicDTO){
        this.title = musicDTO.title();
        this.artist = musicDTO.artist();
        this.genre = musicDTO.genre();
        this.duration = musicDTO.duration();
    }
}
