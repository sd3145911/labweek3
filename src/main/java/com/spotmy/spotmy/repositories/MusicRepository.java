package com.spotmy.spotmy.repositories;

import com.spotmy.spotmy.models.Music;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MusicRepository extends JpaRepository<Music, UUID> {
    Optional<List<Music>> findMusicByArtist(String artist);
    Optional<List<Music>> findMusicByTitle(String title);
    Optional<List<Music>> findMusicByGenre(String genre);
    Optional<List<Music>> findByDurationBetween(int min, int max);
}
