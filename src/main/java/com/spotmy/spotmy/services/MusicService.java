package com.spotmy.spotmy.services;

import com.spotmy.spotmy.dtos.music.RequestMusicDTO;
import com.spotmy.spotmy.dtos.music.ResponseMusicDTO;
import com.spotmy.spotmy.exceptions.EventFoundMusicException;
import com.spotmy.spotmy.models.Music;
import com.spotmy.spotmy.repositories.MusicRepository;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MusicService {
    private final MusicRepository musicRepository;

    public MusicService(MusicRepository musicRepository) {
        this.musicRepository = musicRepository;
    }

    public ResponseMusicDTO createMusic(RequestMusicDTO musicDTO){
        var result = this.musicRepository.save(new Music(musicDTO));

        return genericReturnMusic(result);
    }

    public List<ResponseMusicDTO> getAllMusic(){
        var musics = musicRepository.findAll();

        return  genericReturnList(musics);
    }

    public ResponseMusicDTO editMusic(UUID id, RequestMusicDTO musicDTO){
        var music = musicRepository.findById(id).orElseThrow(EventFoundMusicException::new);

        if(musicDTO.title() != null) music.setTitle(musicDTO.title());
        if(musicDTO.artist() != null) music.setArtist(musicDTO.artist());
        if(musicDTO.genre() != null) music.setGenre(musicDTO.genre());
        if(musicDTO.duration() > 0) music.setDuration(musicDTO.duration());

        var result = musicRepository.save(music);

        return genericReturnMusic(result);
    }

    public void deleteMusic(UUID id){
        if(musicRepository.findById(id).isEmpty()) throw new EventFoundMusicException();

        musicRepository.deleteById(id);
    }

    public ResponseMusicDTO findMusicById(UUID id){
        var result = musicRepository.findById(id).orElseThrow();

        return genericReturnMusic(result);
    }

    public List<ResponseMusicDTO> getMusicByFilter(String title, String artist, String genre, String duration){
        List<Music> musics = new ArrayList<>();

        if (title != null) musics.addAll(musicRepository.findMusicByTitle(title).orElseThrow(EventFoundMusicException::new));
        if (artist != null) musics.addAll(musicRepository.findMusicByArtist(artist).orElseThrow(EventFoundMusicException::new));
        if (genre != null) musics.addAll(musicRepository.findMusicByGenre(genre).orElseThrow(EventFoundMusicException::new));
        if(duration != null) musics.addAll(musicRepository.findByDurationBetween(
                Integer.parseInt(duration), Integer.parseInt(duration)).orElseThrow(EventFoundMusicException::new));


        return genericReturnList(musics);
    }

    private List<ResponseMusicDTO> genericReturnList(List<Music> musics){
        return  musics
                .stream()
                .map(music -> new ResponseMusicDTO(music.getId(), music.getTitle(), music.getArtist(), music.getGenre(), music.getDuration()))
                .collect(Collectors.toList());
    }

    private ResponseMusicDTO genericReturnMusic(Music result){
        return new ResponseMusicDTO(result.getId(), result.getTitle(), result.getArtist(), result.getGenre(), result.getDuration());
    }
}
