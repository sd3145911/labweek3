package com.spotmy.spotmy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpotmyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotmyApplication.class, args);
	}

}
