package com.spotmy.spotmy.controllers;

import com.spotmy.spotmy.dtos.music.RequestMusicDTO;
import com.spotmy.spotmy.dtos.music.ResponseMusicDTO;
import com.spotmy.spotmy.services.MusicService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/music")
public class MusicController {
    private final MusicService musicService;

    public MusicController(MusicService musicService) {
        this.musicService = musicService;
    }

    @PostMapping("/create")
    public ResponseEntity<ResponseMusicDTO> crateMusic(@RequestBody @Valid RequestMusicDTO musicDTO)
    {
        var newMusic = musicService.createMusic(musicDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(newMusic);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMusicDTO> deleteMusic(@PathVariable UUID id){
        musicService.deleteMusic(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping
    public ResponseEntity<List<ResponseMusicDTO>> getAllMusics(){
        return ResponseEntity.ok(musicService.getAllMusic());
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseMusicDTO> editMusic(@PathVariable UUID id, @RequestBody @Valid RequestMusicDTO musicDTO){
        return ResponseEntity.ok(musicService.editMusic(id, musicDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseMusicDTO> findMusicById(@PathVariable UUID id){
        var music = musicService.findMusicById(id);

        return ResponseEntity.ok(music);
    }

    @GetMapping("/search")
    public ResponseEntity<List<ResponseMusicDTO>> searchMusic(@RequestParam(required = false) String title,
                                                   @RequestParam(required = false) String artist,
                                                   @RequestParam(required = false) String genre,
                                                   @RequestParam(required = false) String duration){

        return ResponseEntity.ok(musicService.getMusicByFilter(title, artist, genre, duration));
    }
}
