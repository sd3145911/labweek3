package com.spotmy.spotmy.exceptions;

public class EventFoundMusicException extends RuntimeException{
    public EventFoundMusicException(){
        super("Error to edit music.");
    }

    public EventFoundMusicException(String message){
        super(message);
    }
}
